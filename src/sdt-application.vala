namespace Sdt
{
	public class Application : Gtk.Application
	{
		public Application()
		{
			Object(application_id : "org.gnome.gitlab.mahmoudkhalil.sdt",
				flags: ApplicationFlags.FLAGS_NONE);
		}
	
		protected override void activate()
		{
			var window = new Sdt.ApplicationWindow(this);
			window.set_title("Open A New Project");
			window.set_default_size(300, 300);
			window.show_all();
		}
	}
}
