namespace Sdt
{
	[GtkTemplate (ui = "/org/gnome/sdt/ui/sdt-main-view.ui")]
	class MainView : Gtk.Box
	{
		[GtkChild]
		private Gtk.Button d_add_project_button;
		
		public Gtk.Button add_project_button
		{
			get { return d_add_project_button; }
		}
		
		[GtkChild]
		private Gtk.Entry d_add_project_entry;
		
		public Gtk.Entry add_project_entry
		{
			get { return d_add_project_entry; }
		}
	}
}
