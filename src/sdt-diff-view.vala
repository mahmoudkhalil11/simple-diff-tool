namespace Sdt
{
	[GtkTemplate (ui = "/org/gnome/sdt/ui/sdt-diff-view.ui")]
	class DiffView : Gtk.Box
	{
		[GtkChild]
		private Gtk.Entry d_first_commit_sha_entry;
		
		public Gtk.Entry first_commit_sha_entry
		{
			get { return d_first_commit_sha_entry; }
		}
		
		[GtkChild]
		private Gtk.Entry d_second_commit_sha_entry;
		
		public Gtk.Entry second_commit_sha_entry
		{
			get { return d_second_commit_sha_entry; }
		}
		
		[GtkChild]
		private Gtk.Button d_compare_button;
		
		public Gtk.Button compare_button
		{
			get { return d_compare_button; }
		}
		
		[GtkChild]
		private Gtk.TextView d_diff_text_view;
		
		public Gtk.TextView diff_text_view
		{
			get { return d_diff_text_view; }
		}
	}
}
