# Simple Diff Tool

Simple Diff Tool is a tool that is used to open git repos and detect differences between two commits.

## Building

```
cd simple-diff-tool
meson builddir --prefix=/usr
cd builddir
ninja
./sdt
```
